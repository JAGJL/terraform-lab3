
resource "aws_s3_bucket" "my-bucket-jlt33590"{
   bucket = "my-bucket-jlt-09072196"
   acl = "public-read"
}
resource "aws_s3_bucket_object" "picture-of-cat" {
    bucket =  aws_s3_bucket.my-bucket-jlt33590.id
    key = "cat.jpg"
    acl = "public-read"
    source = "./cat.jpg"
}